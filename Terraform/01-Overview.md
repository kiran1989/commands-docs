## Terraform Overview
* Terraform language is declarative, describing an intended goal rather than the steps to reach that goal.
* Terraform only considers implicit and explicit relationships between resources when determining an order of operations.
* Terraform language tell Terraform what plugins to install, what infrastructure to create, and what data to fetch.
* Terraform language is stored in plain text files with the .tf file extension. There is also a JSON-based variant of the language that is named with the .tf.json file extension.

The syntax of the Terraform language consists of only a few basic elements:
```
resource "aws_vpc" "main-01" {
  cidr_block = var.base_cidr_block
}

<BLOCK TYPE> "<BLOCK LABEL>" "<BLOCK LABEL>" {
  # Block body
  <IDENTIFIER> = <EXPRESSION> # Argument
}
```
* Resource names must start with a letter or underscore, and may contain only letters, digits, underscores, and dashes. Eg. above `main-01`.
### Providers
A provider is a plugin for Terraform that offers a collection of resource types.
### Override Files
Terraform has special handling of any configuration file whose name ends in _override.tf or _override.tf.json or override.tf or override.tf.json
### Lock File
Terraform automatically creates or updates the dependency lock file each time you run the `terraform init`.The lock file is always named `.terraform.lock.hcl` it is a lock file for various items that Terraform caches in the `.terraform` subdirectory.
* If a particular provider has no existing recorded selection, Terraform will select the newest available version and then update the lock file.
* If a particular provider already has a selection recorded in the lock file, Terraform will always re-select that version for installation, even if a newer version is available.
### Test Files
* Terraform test files use the file extensions `.tftest.hcl` and `.tftest.json`.
* Terraform loads all test files within your root configuration directory. Override the location by appending the `-test-directory` flag. The default testing directory is `tests`.
### Comments
* `#` begins a single-line comment, ending at the end of the line.
* `//` also begins a single-line comment, as an alternative to #.
* `/*` and `*/` are start and end delimiters for multiple lines.
### Meta-Arguments
The following meta-arguments, which can be used with any resource type to change the behavior of resources. `depends_on`, `count`, `for_each`, `provider` `lifecycle` and `provisioner`.
### Removing Resources
The `removed block` is available. For earlier Terraform versions, you can use the `terraform state rm` CLI command. Sometimes you may wish to remove a resource from your Terraform configuration without destroying the real infrastructure.
```
removed {
  from = aws_instance.example

  lifecycle {
    destroy = false
  }
}
```
### Operation Timeouts
Only Some resource types provide a special timeouts nested block argument that allows you to customize how long certain operations are allowed to take before being considered to have failed.
```
resource "aws_db_instance" "example" {
  timeouts {
    create = "60m"
    delete = "2h"
  }
}
```
### Resource Dependencies
* Terraform can make changes to several unrelated resources in parallel.
* Most resource dependencies are handled automatically. Still the `depends_on` meta-argument can explicitly specify a dependency.
& The `replace_triggered_by` meta-argument it forces Terraform to replace the parent resource when there is a change in resource or resource attribute
### Local-only Resources
Certain specialized resource types that operate only within Terraform itself. Eg. `generating private keys`, `issuing self-signed TLS certificates`, and even generating `random ids`.
### Data Sources
Data sources allow Terraform to use information defined outside of Terraform
* Terraform reads data resources during the planning phase when possible.
* Data resources have almost same dependency resolution behavior as resource block.
```
data "aws_ami" "example" {
  most_recent = true

  owners = ["self"]
  tags = {
    Name   = "app-server"
    Tested = "true"
  }
}
```
### provider 
```
# default configuration
provider "google" {
  region = "us-central1"
}

# alternate configuration, whose alias is "europe"
provider "google" {
  alias  = "europe"
  region = "europe-west1"
}

resource "google_compute_instance" "example" {
  # This "provider" meta-argument selects the google provider
  # configuration whose alias is "europe", rather than the
  # default configuration.
  provider = google.europe
}
```
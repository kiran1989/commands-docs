## Terrafrom Variables
The name of a variable can be any valid identifier except the following: `source`, `version`, `providers`, `count`, `for_each`, `lifecycle`, `depends_on`, `locals`. These names are reserved for meta-arguments.

Following optional arguments in the variable declaration:

`default` - default value will be used if no value is set

`type` - if no type constraint is set then a value of any type is accepted. eg `string`, `number`, `boolean` eg Complex var types `list(<TYPE>)` `set(<TYPE>)` `map(<TYPE>)` `object({<ATTR NAME> = <TYPE>, ... })` `tuple([<TYPE>, ...])`

`description` - This specifies the input variable's documentation.

`validation` - A block to define validation rules, usually in addition to type constraints.

`sensitive` - Sensitive prevents from showing its value in plan

`nullable` - Specify if the variable can be null within the module.
### String
Commonly used to assign single value. Sensitive prevents Terraform from showing its value in plan
```
variable "pass" {
  type = string
  default = "12123abcd"
  value = "asdsad"
  sensitive = true
}
```
Example Usage
```
name = var.app_name
```
### Number
The number type can represent both whole numbers like 15 and fractional values like 6.283185
```
variable "num" {
  type = number
  default = 123.12
  value = 123.243
}
```
Example Usage
```
number = var.num
```
### Boolean
Used for simple true or false values
```
variable "deploy" {
  default = false
}
```
Example Usage
```
can_deploy = var.deploy
```
### List or Tuple
List can store elements of different types, but arrays can store elements only of the same type. Have corresponding index. values like ["a", 15, true]
```
variable "users" {
  type    = list
  default = ["root", 123, true]
}
```
Example Usage
```
users = var.users[0]
```
### Map or Object
Map or objects is a collection of key-value pairs represented by a pair of curly braces. Key/value pairs can be separated by either a comma or a line break.
```
variable "release" {
  type = map
  default = {
    "dev" = 1.21.3
    "acc" = 1.21.4
    "prd" = 1.21.9
  }
}

variable "storage_sizes" {
  type = map
  default = {
    "1.21.3"  = "25"
    "1.21.4"  = "50"
    "1.21.9"  = "80"
  }
}
```
Example Usage
```
ver = var.release["acc"]
```
You can also use lookup function to retrieves the value from complex maps example below.
```
size = lookup(var.storage_sizes, var.plans["dev"])
```
### Example complex variable
```
variable "docker_ports" {
  type = list(object({
    internal = number
    external = number
    protocol = string
  }))
  default = [
    {
      internal = 8300
      external = 8300
      protocol = "tcp"
    }
  ]
}
```
### Set
Terraform language doesn't have a literal syntax for set values, but you can use the toset function to explicitly convert a list of strings to a set.
* key and value are the same for a set
* Conversion from list to set discards the ordering of the items in the list and removes any duplicate elements. toset(["b", "a", "b"]) will produce a set containing only "a" and "b".
```
locals {
  subnet_ids = toset([
    "subnet-abcdef",
    "subnet-012345",
  ])
}

resource "aws_instance" "server" {
  for_each = local.subnet_ids

  subnet_id     = each.key # note: each.key and each.value are the same for a set

  tags = {
    Name = "Server ${each.key}"
  }
}
```
```
variable "subnet_ids" {
  type = set(string)
}

resource "aws_instance" "server" {
  for_each = var.subnet_ids
}
```
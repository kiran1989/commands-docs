## Meta-Arguments
### depends_on
* To handle hidden `resource` or `module` dependencies that Terraform cannot automatically identify.
* Instead of `depends_on`, we recommend using [expression references](https://developer.hashicorp.com/terraform/language/expressions/references) to imply dependencies when possible
```
resource "aws_instance" "example" {
  ami           = "ami-a1b2c3d4"
  instance_type = "t2.micro"

  depends_on = [
    aws_iam_role_policy.example
  ]
}
```
```
output "vpc_ids" {
  value = aws_instance.example.id
  }
  depends_on = [aws_internet_gateway.example]
}
```
### count
* A resource or module block cannot use `count` and `for_each` at same time.
```
resource "aws_instance" "server" {
  count = 4

  tags = {
    Name = "Server ${count.index}"
    }
}
```
```
variable "subnet_ids" {
  type = list(string)
}

resource "aws_instance" "server" {
  count = length(var.subnet_ids)

  instance_type = "t2.micro"
  subnet_id     = var.subnet_ids[count.index]
}
```
`insid = aws_instance.server[0].id` 

When to Use `for_each` Instead of `count`
* If your instances are identical `count` is fine. If some of their arguments need distinct values then it's safer to use `for_each`.
### for_each
* A resource or module block cannot use `count` and `for_each` at same time.
* Used to manage several similar objects without writing a separate block for each one.
* The `for_each` accepts a map or a set of strings.
```
resource "azurerm_resource_group" "rg" {
  for_each = tomap({
    a_group   = "eastus"
    b_group   = "westus2"
  })
  name     = each.key     #a_group
  location = each.value   #eastus
}
```
`agroup = azurerm_resource_group.rg["a_group"]`
```
module "bucket" {
  for_each = toset(["assets", "media"])
  source   = "./create_bucket"
  name     = "${each.key}_bucket"
}
```
Limitations on values used in `for_each`
* The keys and values must be specified.
* Connot use for of impure functions, including `uuid`, `bcrypt`, or `timestamp`.
* Sensitive values, such as sensitive input variables, sensitive outputs, or sensitive resource attributes, cannot be used as arguments to `for_each`.
```
variable "vpcs" {
  type = map(object({
    cidr_block = string
  }))
}

resource "aws_vpc" "example" {
  for_each = var.vpcs
  cidr_block = each.value.cidr_block
}

output "vpc_ids" {
  value = {
    for k, v in aws_vpc.example : k => v.id
  }
}
```
### lifecycle
`lifecycle` is a nested block that can appear within a resource block, available for all resource blocks regardless of type.
* `create_before_destroy` (bool)
* `prevent_destroy` (bool)
* `ignore_changes`  (list of attribute names) eg. `ignore_changes = [tags,]`
* `replace_triggered_by` (list of resource or attribute references)
```
resource "azurerm_resource_group" "example" {
  lifecycle {
    create_before_destroy = true
  }
}

```
### Custom Condition Checks
`precondition` and `postcondition` blocks with a `lifecycle` are the custom checks
```
resource "aws_instance" "example" {
  instance_type = "t2.micro"
  ami           = "ami-abc123"

  lifecycle {
    precondition {
      condition     = data.aws_ami.example.architecture == "x86_64"
      error_message = "AMI must be for the x86_64 architecture."
    }
  }
}
```
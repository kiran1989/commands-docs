## Provisioners
You can use provisioners to model specific actions on the local machine or on a remote machine. Terraform includes the concept of provisioners as that there are always certain behaviors that cannot be directly represented in Terraform's declarative model.
* Terraform cannot model the actions of provisioners as part of a plan.
* Use provisioners only if there is no other option.
* The `self` object represents the provisioner's parent resource, and has all of that resource's attributes. For example, use `self.public_ip` to reference an `aws_instance's public_ip` attribute.
* All log output from the provisioner is automatically suppressed to prevent the sensitive values from being displayed.
### Destroy-Time Provisioners
```
resource "aws_instance" "web" {
  provisioner "local-exec" {
    when    = destroy
    command = "echo 'Destroy-time provisioner'"
  }
}
```
### Failure Behavior
* `continue` - Ignore the error and continue with creation or destruction.
* `fail` - Raise an error and stop applying (the default behavior).
```
resource "aws_instance" "web" {

  provisioner "local-exec" {
    command    = "echo The server's IP address is ${self.private_ip}"
    on_failure = continue
  }
}
```
### file 
The file provisioner copies files or directories from the machine running Terraform to the newly created resource.
```
resource "aws_instance" "web" {
  # Copies the myapp.conf file to /etc/myapp.conf
  provisioner "file" {
    source      = "conf/myapp.conf"
    destination = "/etc/myapp.conf"
  }

  # Copies all files and folders in apps/app1 to D:/IIS/webapp1
  provisioner "file" {
    source      = "apps/app1/"
    destination = "D:/IIS/webapp1"
  }
}
```
### local-exec
The `local-exec` provisioner invokes a local executable after a resource is created. This invokes a process on the machine running Terraform.
```
resource "aws_instance" "web" {
  provisioner "local-exec" {
    command = "echo ${self.private_ip} >> private_ips.txt"
  }
}
```
### local-exec
The `remote-exec` provisioner invokes a script on a remote resource after it is created. This can be used to run a configuration management tool, bootstrap into a cluster.
```
resource "aws_instance" "web" {
  # Establishes connection to be used by all
  # generic remote provisioners (i.e. file/remote-exec)
  connection {
    type     = "ssh"
    user     = "root"
    password = var.root_password
    host     = self.public_ip
  }

  provisioner "remote-exec" {
    inline = [
      "puppet apply",
      "consul join ${aws_instance.web.private_ip}",
    ]
  }
}
```
## Provisioners Without a Resource
If you need to run provisioners that aren't directly associated with a specific resource, you can use `terraform_data`.
* The `terraform_data` implements the standard resource lifecycle.
* The `terraform_data` resource is useful for storing values which need to follow and manage resource lifecycle.
* Used for `null_resource` replacement
```
resource "aws_instance" "cluster" {
  count = 3
}

resource "terraform_data" "cluster" {
  # Replacement of any instance of the cluster requires re-provisioning
  triggers_replace = aws_instance.cluster.[*].id

  connection {
    host = aws_instance.cluster.[0].public_ip
  }

  provisioner "remote-exec" {
    # Bootstrap script called with private_ip of each node in the cluster
    inline = [
      "bootstrap-cluster.sh ${join(" ", aws_instance.cluster.*.private_ip)}",
    ]
  }
}
```
```
variable "revision" {
  default = 1
}

resource "terraform_data" "replacement" {
  input = var.revision
}

# This resource has no convenient attribute which forces replacement,
# but can now be replaced by any change to the revision variable value.
resource "example_database" "test" {
  lifecycle {
    replace_triggered_by = [terraform_data.replacement]
  }
}
```

## Modules
* A module is a collection of .tf and/or .tf.json files kept together in a directory. Nested directories are treated as completely separate modules.
## Root Module
* The root module is the working directory where Terraform is invoked.
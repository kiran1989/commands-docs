import os
from kubernetes import client, config

#config.load_kube_config(
#    os.path.join(os.environ["HOME"], './config'))
aToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE1ODY1NDA4MDMsImdyb3VwcyI6WyJyX2s4c19kY3BfZGV2X3J3Iiwicl9rOHNfZGNwX3ByZF9ydyIsInJfazhzX2RjcF9zdGdfYWRtaW4iLCJyX2s4c19kY3Bfc3RnX3J3Iiwicl9rOHNfZGNwX3BsYl9ydyIsInJfcmhlbF9kY3BfcHJkX3JvIiwicl9zb25hcnF1YmVfZGNwX3ByZF9kZXZvcHMiLCJyX2dyYWZhbmFfZGNwX3ByZF9ydyIsInJfY29uZmlnX2RjcF9wcmRfdXNlciIsInJfY29uZmlnX2RjcF9wcmRfYWRtaW4iLCJyX2dyYWZhbmFfZGNwX3N0Z19ydyIsInJfamVua2luc19kY3BfZGV2X2Rldm9wcyIsInJfamVua2luc19kY3BfcHJkX2Rldm9wcyIsInJfYml0YnVja2V0X2RjcF9wcmRfb3BzIiwicl9qZW5raW5zX2RjcF9kZXZfYWRtaW4iLCJyX2JpdGJ1Y2tldF9kY3BfZGV2X2FkbWluIiwicl9iaXRidWNrZXRfZGNwX2Rldl9kZXZvcHMiLCJyX21hcmF0aG9uX2NjcF9kY3BfYWRtaW4iLCJyX2FydGlmYWN0b3J5X2RjcF9wcmRfYWRtaW4iLCJyX2plbmtpbnNfZGNwX3ByZF9hZG1pbiIsInJfamVua2luc19kY3BfcHJkX2RldmVsb3BlcnMiLCJyX2JpdGJ1Y2tldF9kY3BfcHJkX2Rldm9wcyIsInJfYml0YnVja2V0X2RjcF9wcmRfZGV2ZWxvcGVycyIsInJfcmhlbF9kY3BfcHJkX2FkbWluIiwicl9yaGVsX2RjcF9wcmRfc3VwcG9ydCIsInJfcmhlbF9kY3BfZGV2X3N1cHBvcnQiLCJyX3JoZWxfZGNwX2Rldl9hZG1pbiJdLCJpYXQiOjE1ODY0NTQ0MDMsImlzcyI6ImNvbmR1Y2t0b3IiLCJ1aWQiOiJLUmFqZW5kNyIsInVzZXJuYW1lIjoia3JhamVuZDcifQ.Vs6UyCUB6F8j1J9a1jSXTjg0a3bQ_9Ae2GvDMo_rp94"
aConfiguration = client.Configuration()
aConfiguration.host = "https://stg.dcp.master.kube.kiran.com:6443"
aConfiguration.verify_ssl = False
aConfiguration.api_key = {"authorization": "Bearer " + aToken}
aApiClient = client.ApiClient(aConfiguration)
v1 = client.CoreV1Api(aApiClient)

#v1 = client.CoreV1Api()

pod_list = v1.list_namespaced_pod("dcp-common-dcp-stg")
for pod in pod_list.items:
    print("%s\t%s\t%s" % (pod.metadata.name, 
                          pod.status.phase,
                          pod.status.pod_ip))